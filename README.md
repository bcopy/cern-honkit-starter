![Build Status](https://gitlab.cern.ch/YOUR_GITLAB_GROUP/YOUR_GITLAB_REPO/badges/master/pipeline.svg)

# Purpose

This document outlines ....


Currently available behind CERN SSO at :
https://YOUR_GITLAB_PAGES_HOST.docs.cern.ch

# How to use this template

You can use ``npx tiged https://gitlab.cern.ch/bcopy/cern-honkit-starter.git my-report``, or simply fork it in Gitlab.

Then you need to update :
* ``package.json``
* ``book.json``
* ``assets/data.ods``

The template uses the following Honkit plugins :
* 

# How to update this document

The document is produced with ~~Gitbook~~ Honkit.

## Edit and preview locally

* Prerequisites
  * You will need a JDK v11+

* You can use the provided Gradle wrapper to install node locally (Alternatively you must have node v14+ installed)
  * ```./gradlew installNode``` on Linux / Mac OSX
  * or ```gradlew.bat installNode``` on Windows
* You can then setup your path and install the required dependencies :
  * If using the gradle wrapper : ```source ./setup-path.sh```
  * ```npm install```
* Finally you can run the book in preview mode
  * ```npm run serve```

